import quandl
import pandas as pd
import math
from sklearn import preprocessing,cross_validation,svm
from sklearn.linear_model import LinearRegression
import numpy as np
from datetime import date,timedelta,datetime
import matplotlib.pyplot as plt
from matplotlib import style


style.use('ggplot')

df= quandl.get("YAHOO/NS_SAIL", authtoken="TsAsYMCp7AaMk6GuWxXK")

print(df.head())
df=df[['Open','Low','High','Close','Volume',]]

df['HL_PCT']=(df['High']-df['Low']/df['Close']*100.0)
df['PCT_change']=(df['Close']-df['Open']/df['Open']*100.0)

df=df[['Close','HL_PCT','PCT_change','Volume']]
#df['Forecast'] = np.nan

#the cloumn that is considered to be the label(forecasted)
forecast_col='Close'
df.fillna(-9999,inplace=True)

#the forecast_date into the future
forecast_out=int(math.ceil(0.01*len(df)))

#print(forecast_out)


df['label']=df[forecast_col].shift(-forecast_out)
#df.dropna(inplace=True)
#print(df.tail())

x=np.array(df.drop(['label'],1))
#y=np.array(df['label'])
#print(len(x))

x=preprocessing.scale(x)
#y=np.array(df['label'])

x_lately=x[-forecast_out:]
x=x[:-forecast_out]

df.dropna(inplace=True)

y=np.array(df['label'])

print (len(x))
print (len(y))

x_train,x_test,y_train,y_test=cross_validation.train_test_split(x,y,test_size=0.25)


clf = LinearRegression()

clf.fit(x_train,y_train)

confidence=clf.score(x_test,y_test)

#print("score for LinearRegression:",confidence)
forecast_set = clf.predict(x_lately)

print(forecast_set,confidence,forecast_out)


"""for k in ['linear','poly']:
  clf=svm.SVR(kernel=k)
  clf.fit(x_train,y_train)
  confidence=clf.score(x_test,y_test)
  print("the score for ",k," is :",confidence)
"""

df['Forecast']=np.nan

last_date=df.iloc[-1].name
print(type(last_date))
last_unix = last_date.value
one_day=86400
next_unix=last_unix + one_day

for i in forecast_set:
    next_date=last_date + timedelta(seconds=86400)
    last_date=next_date
    df.loc[next_date] = [np.nan for _ in range(len(df.columns)-1)]+[i]

df['Close'].plot()
df['Forecast'].plot()
plt.legend(loc=4)
plt.xlabel('Date')
plt.ylabel('Price')
plt.show()


